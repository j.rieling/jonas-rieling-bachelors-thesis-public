import sys
from multiprocessing import Process
import pandas as pd
import os
import math
#sys.path.append('./py_util')

from snsynth import Synthesizer as s
from snsynth.transform import *

# all variables initially used in analysis
def aline():
    return pd.read_csv("original_data/aline_clean.csv", index_col=None)

def save(df, synthesizer_name, params, eps, prep_eps = ""):

    eps = str(eps)
    if(prep_eps != ""): prep_eps = "_p" + str(prep_eps)

    loc = "synth_data/{0}/{1}_{2}_eps{3}{4}.csv".format(synthesizer_name, params, synthesizer_name, eps, prep_eps)

    run = 1
    while os.path.isfile(loc):
        loc = "synth_data/{0}/{1}_{2}_eps{3}{4}_run{5}.csv".format(synthesizer_name, params, synthesizer_name, eps, prep_eps, str(run))
        run = run + 1

    df.to_csv(loc)

# neural networks need OneHot encoding
def tt_gan():
    return TableTransformer([
        MinMaxTransformer(lower=16, upper=92), # age
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # gender
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # day_icu_intime
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # hour_icu_intime
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # icu_exp_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # day_28_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # aline_flag
        MinMaxTransformer(lower=25, upper=250),# weight_first
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # service_unit
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # sofa_first
        MinMaxTransformer(lower=10, upper=200),# map_first
        MinMaxTransformer(lower=25, upper=200),# hr_first
        MinMaxTransformer(lower=30, upper=45),# temp_first
        MinMaxTransformer(lower=30, upper=100),# spo2_first
        MinMaxTransformer(lower=1, upper=150),# bun_first
        MinMaxTransformer(lower=0, upper=20), # creatinine_first
        MinMaxTransformer(lower=60, upper=140), # chloride_first
        MinMaxTransformer(lower=2, upper=25), # hgb_first
        MinMaxTransformer(lower=0, upper=900), # platelet_first
        MinMaxTransformer(lower=1, upper=10), # potassium_first
        MinMaxTransformer(lower=100, upper=180), # sodium_first
        MinMaxTransformer(lower=0, upper=60), # tco2_first
        MinMaxTransformer(lower=0, upper=100), # wbc_first
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # chf_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # afib_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # renal_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # liver_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # copd_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # cad_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # stroke_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # malignancy_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]) # respfail_flag
    ])

# pac is a marginal, but can also deliver NULLs
def tt_pac():
    return TableTransformer([
        BinTransformer(bins=30, lower=16, upper=92,nullable=True), # age 0--
        LabelTransformer(nullable=True), # gender 1--
        LabelTransformer(nullable=True), # day_icu_intime 2--
        LabelTransformer(nullable=True), # hour_icu_intime 3--
        LabelTransformer(nullable=True), # icu_exp_flag 4--
        LabelTransformer(nullable=True), # day_28_flag 5--
        LabelTransformer(nullable=True), # aline_flag 6--
        BinTransformer(bins=30, lower=25, upper=250, nullable=True),# weight_first 7--
        LabelTransformer(nullable=True), # service_unit 8--
        LabelTransformer(nullable=True), # sofa_first 9--
        BinTransformer(bins=30, lower=10, upper=200, nullable=True),# map_first 10--
        BinTransformer(bins=30, lower=25, upper=200, nullable=True),# hr_first 11--
        BinTransformer(bins=30, lower=30, upper=45, nullable=True),# temp_first 12--
        BinTransformer(bins=30, lower=30, upper=100, nullable=True),# spo2_first 13-
        BinTransformer(bins=30, lower=1, upper=150, nullable=True),# bun_first 14--
        BinTransformer(bins=30, lower=0, upper=20, nullable=True), # creatinine_first 15--
        BinTransformer(bins=30, lower=60, upper=140, nullable=True), # chloride_first 16--
        BinTransformer(bins=30, lower=2, upper=25, nullable=True), # hgb_first 17--
        BinTransformer(bins=30, lower=0, upper=900, nullable=True), # platelet_first 18--
        BinTransformer(bins=30, lower=1, upper=10, nullable=True), # potassium_first 19--
        BinTransformer(bins=30, lower=100, upper=180, nullable=True), # sodium_first 20--
        BinTransformer(bins=30, lower=0, upper=60, nullable=True), # tco2_first 21--
        BinTransformer(bins=30, lower=0, upper=100, nullable=True), # wbc_first 22--
        LabelTransformer(nullable=True), # chf_flag 23 --
        LabelTransformer(nullable=True), # afib_flag 24 --
        LabelTransformer(nullable=True), # renal_flag 25 --
        LabelTransformer(nullable=True), # liver_flag 26 --
        LabelTransformer(nullable=True), # copd_flag 27 --
        LabelTransformer(nullable=True), # cad_flag 28--
        LabelTransformer(nullable=True), # stroke_flag 29--
        LabelTransformer(nullable=True), # malignancy_flag 30 --
        LabelTransformer(nullable=True) # respfail_flag 31--
    ])

# neural networks need OneHot encoding
def tt_gan():
    return TableTransformer([
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # day_28_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # aline_flag
        MinMaxTransformer(lower=25, upper=250),# weight_first
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # service_unit
        MinMaxTransformer(lower=10, upper=200),# map_first
        MinMaxTransformer(lower=25, upper=200),# hr_first
        MinMaxTransformer(lower=30, upper=45),# temp_first
        MinMaxTransformer(lower=30, upper=100),# spo2_first
        MinMaxTransformer(lower=1, upper=150),# bun_first
        MinMaxTransformer(lower=0, upper=20), # creatinine_first
        MinMaxTransformer(lower=60, upper=140), # chloride_first
        MinMaxTransformer(lower=2, upper=25), # hgb_first
        MinMaxTransformer(lower=0, upper=900), # platelet_first
        MinMaxTransformer(lower=1, upper=10), # potassium_first
        MinMaxTransformer(lower=100, upper=180), # sodium_first
        MinMaxTransformer(lower=0, upper=60), # tco2_first
        MinMaxTransformer(lower=0, upper=100), # wbc_first
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # chf_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # afib_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # renal_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # liver_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # copd_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # cad_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # stroke_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # malignancy_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]) # respfail_flag
    ])

# Marginals only take categories, which can best be abstracted as histograms/ bins
def tt_marginal():
    return TableTransformer([
        BinTransformer(bins=30, lower=16, upper=92), # age 0--
        LabelTransformer(nullable=False), # gender 1--
        LabelTransformer(nullable=False), # day_icu_intime 2--
        LabelTransformer(nullable=False), # hour_icu_intime 3--
        LabelTransformer(nullable=False), # icu_exp_flag 4--
        LabelTransformer(nullable=False), # day_28_flag 5--
        LabelTransformer(nullable=False), # aline_flag 6--
        BinTransformer(bins=30, lower=25, upper=250),# weight_first 7--
        LabelTransformer(nullable=False), # service_unit 8--
        LabelTransformer(nullable=False), # sofa_first 9--
        BinTransformer(bins=30, lower=10, upper=200),# map_first 10--
        BinTransformer(bins=30, lower=25, upper=200),# hr_first 11--
        BinTransformer(bins=30, lower=30, upper=45),# temp_first 12--
        BinTransformer(bins=30, lower=30, upper=100),# spo2_first 13-
        BinTransformer(bins=30, lower=1, upper=150),# bun_first 14--
        BinTransformer(bins=30, lower=0, upper=20), # creatinine_first 15--
        BinTransformer(bins=30, lower=60, upper=140), # chloride_first 16--
        BinTransformer(bins=30, lower=2, upper=25), # hgb_first 17--
        BinTransformer(bins=30, lower=0, upper=900), # platelet_first 18--
        BinTransformer(bins=30, lower=1, upper=10), # potassium_first 19--
        BinTransformer(bins=30, lower=100, upper=180), # sodium_first 20--
        BinTransformer(bins=30, lower=0, upper=60), # tco2_first 21--
        BinTransformer(bins=30, lower=0, upper=100), # wbc_first 22--
        LabelTransformer(nullable=False), # chf_flag 23 --
        LabelTransformer(nullable=False), # afib_flag 24 --
        LabelTransformer(nullable=False), # renal_flag 25 --
        LabelTransformer(nullable=False), # liver_flag 26 --
        LabelTransformer(nullable=False), # copd_flag 27 --
        LabelTransformer(nullable=False), # cad_flag 28--
        LabelTransformer(nullable=False), # stroke_flag 29--
        LabelTransformer(nullable=False), # malignancy_flag 30 --
        LabelTransformer(nullable=False) # respfail_flag 31--
    ])

# pac is a marginal, but can also deliver NULLs
def tt_pac():
    return TableTransformer([
        BinTransformer(bins=30, lower=16, upper=92,nullable=True), # age 0--
        LabelTransformer(nullable=True), # gender 1--
        LabelTransformer(nullable=True), # day_icu_intime 2--
        LabelTransformer(nullable=True), # hour_icu_intime 3--
        LabelTransformer(nullable=True), # icu_exp_flag 4--
        LabelTransformer(nullable=True), # day_28_flag 5--
        LabelTransformer(nullable=True), # aline_flag 6--
        BinTransformer(bins=30, lower=25, upper=250, nullable=True),# weight_first 7--
        LabelTransformer(nullable=True), # service_unit 8--
        LabelTransformer(nullable=True), # sofa_first 9--
        BinTransformer(bins=30, lower=10, upper=200, nullable=True),# map_first 10--
        BinTransformer(bins=30, lower=25, upper=200, nullable=True),# hr_first 11--
        BinTransformer(bins=30, lower=30, upper=45, nullable=True),# temp_first 12--
        BinTransformer(bins=30, lower=30, upper=100, nullable=True),# spo2_first 13-
        BinTransformer(bins=30, lower=1, upper=150, nullable=True),# bun_first 14--
        BinTransformer(bins=30, lower=0, upper=20, nullable=True), # creatinine_first 15--
        BinTransformer(bins=30, lower=60, upper=140, nullable=True), # chloride_first 16--
        BinTransformer(bins=30, lower=2, upper=25, nullable=True), # hgb_first 17--
        BinTransformer(bins=30, lower=0, upper=900, nullable=True), # platelet_first 18--
        BinTransformer(bins=30, lower=1, upper=10, nullable=True), # potassium_first 19--
        BinTransformer(bins=30, lower=100, upper=180, nullable=True), # sodium_first 20--
        BinTransformer(bins=30, lower=0, upper=60, nullable=True), # tco2_first 21--
        BinTransformer(bins=30, lower=0, upper=100, nullable=True), # wbc_first 22--
        LabelTransformer(nullable=True), # chf_flag 23 --
        LabelTransformer(nullable=True), # afib_flag 24 --
        LabelTransformer(nullable=True), # renal_flag 25 --
        LabelTransformer(nullable=True), # liver_flag 26 --
        LabelTransformer(nullable=True), # copd_flag 27 --
        LabelTransformer(nullable=True), # cad_flag 28--
        LabelTransformer(nullable=True), # stroke_flag 29--
        LabelTransformer(nullable=True), # malignancy_flag 30 --
        LabelTransformer(nullable=True) # respfail_flag 31--
    ])

# specify GAN synthesizer, epsilon and preprocessing budget
def gan_dyn(gan_n, eps, prep_eps):
    df = aline()
    for i in range(len(eps)):
        synthesizer = s.create(
            gan_n,
            epsilon = eps[i]
        )
        save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=prep_eps[i]
            ),
            gan_n,
            "dyn",
            eps[i],
            prep_eps[i]
        )

# takes static bounds from tt_gan transformer
def gan_stat(gan_n, eps, prep_eps):
    df = aline()
    prep_eps = 0.0
    for i in range(len(eps)):
        synthesizer = s.create(
            gan_n,
            epsilon=eps[i]
        )
        save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=0.0,
                transformer=tt_gan()
            ),
            gan_n,
            "stat",
            eps[i]
        )

# saves only one df, call multiple times
def pacsynth_dyn(eps, prep_eps):
    df = aline()
    synthesizer = s.create(
        'pacsynth',
        epsilon=eps
    )
    save(
        synthesizer.fit_sample(
            df,
            preprocessor_eps=prep_eps
        ),
        'pacsynth',
        'dyn',
        eps,
        prep_eps
    )

def pacsynth_stat(eps):
    df = aline()
    synthesizer = s.create(
        'pacsynth',
        epsilon=eps,
        delta=1.0 / (math.log(len(df)) * len(df)), # old default
        percentile_percentage=95, # throw out more ouliers than default
        percentile_epsilon_proportion=0.05,
        reporting_length=3 # maximum possible
    )
    save(
        synthesizer.fit_sample(
            df,
            transformer=tt_pac(),
            preprocessor_eps=0.0
        ),
        'pacsynth',
        'stat',
        eps
    )

def mwem_dyn(eps, prep_eps):
    df = aline()
    for i in range(len(eps)):
        synthesizer = s.create(
            'mwem',
            epsilon = eps[i],
            split_factor=3,
        )
        save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=prep_eps[i]
            ),
            'mwem',
            'dyn',
            eps[i],
            prep_eps[i]
        )

def mwem_stat(eps, prep_eps):
    df = aline()
    prep_eps = 0.0
    for i in range(len(eps)):
        synthesizer = s.create(
            'mwem',
            epsilon=eps[i],
            split_factor=3
        )
        save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=0.0,
                transformer=tt_marginal()
            ),
            'mwem',
            "stat",
            eps[i]
        )

def mst_dyn(eps, prep_eps):
    df = aline()
    for i in range(len(eps)):
        synthesizer = s.create(
            'mst',
            epsilon = eps[i],
        )
        save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=prep_eps[i]
            ),
            'mst',
            'dyn',
            eps[i],
            prep_eps[i]
        )

def mst_stat(eps, prep_eps):
    df = aline()
    prep_eps = 0.0
    for i in range(len(eps)):
        synthesizer = s.create(
            'mst',
            epsilon=eps[i],
        )
        save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=0.0,
                transformer=tt_marginal()
            ),
            'mst',
            "stat",
            eps[i]
        )


# usage: call with .
# $python main_exec.py <synthesizer> <dyn/stat>
if __name__ == '__main__':

    epsilon = [
        0.3, # NIST challenge
        1.0, # NIST challenge
        3.0, # lots of default epsilons are at 3
        8.0, # NIST challenge
        20.0 # because we have a lot of columns
    ]

    # preprocessing budget in the usage examples ranged from 33% to 25%
    prep_eps = [
        0.1,
        0.3,
        1.0,
        3.0,
        5.0
    ]

    run = str(sys.argv[1])
    params = str(sys.argv[2]) # dyn or stat
    if(run not in s.list_synthesizers()): exit("'" + run + "' not a synthesizer")
    if(params not in ["dyn","stat"]): exit("'" + params + "' not understandable have a great day")

    if('gan' in run):
        runs = [None]*10
        for i in range(len(runs)):
            runs[i] = Process(target=eval("gan_" + params), args=(run, epsilon, prep_eps))
        for i in range(len(runs)):
            runs[i].start()

    # PAC does not allow pickeling, so no Process :/
    if('pacsynth' == run):
        for i in range(len(epsilon)):
            for j in range(10):
                if(params == "dyn"):
                    pacsynth_dyn(epsilon[i], prep_eps[i])
                if(params == "stat"):
                    pacsynth_stat(epsilon[i])
                j = j+1

    if('mwem' == run):
        runs = [None]*10
        for i in range(len(runs)):
            if(params == "dyn"):
                runs[i] = Process(target=mwem_dyn, args=(epsilon, prep_eps))
            else:
                runs[i] = Process(target=mwem_stat, args=(epsilon, prep_eps))
        for i in range(len(runs)):
            runs[i].start()

    if('mst' == run):
        runs = [None]*10
        for i in range(len(runs)):
            if(params == "dyn"):
                runs[i] = Process(target=mst_dyn, args=(epsilon, prep_eps))
            else:
                runs[i] = Process(target=mst_stat, args=(epsilon, prep_eps))
        for i in range(len(runs)):
            runs[i].start()
