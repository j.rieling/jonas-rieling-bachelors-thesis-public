import sys
from multiprocessing import Process
import pandas as pd
import os
import math
#sys.path.append('./py_util')

from snsynth import Synthesizer as s
from snsynth.transform import *

# selection after glm and step-wise aic
def aic_aline():
    df = pd.read_csv("original_data/aline_clean.csv", index_col=None)
    sub = df[[
        "aline_flag", #-
        "day_28_flag", #-
        "weight_first", #-
        "platelet_first", #-
        "renal_flag", #-
        "liver_flag", #-
        "tco2_first", #-
        "copd_flag", #-
        "map_first",#-
        "sodium_first",#-
        "wbc_first",#-
        "sofa_first",#-
        "stroke_flag",#-
        "chloride_first",#-
        "service_unit" #-
    ]]
    # in final prediction model, service unit only distinguishes surgigal service
    sub['service_unit'] = sub['service_unit'].apply(
        lambda x: 1 if x=="SURG" else 0
    )
    sub.rename(columns={"service_unit":"service_surg"}, inplace=True)
    return sub

def aic_save(df, synthesizer_name, params, eps, prep_eps = ""):

    eps = str(eps)
    if(prep_eps != ""): prep_eps = "_p" + str(prep_eps)

    loc = "aic_synth_data/{0}/aic_{1}_{2}_eps{3}{4}.csv".format(synthesizer_name, params, synthesizer_name, eps, prep_eps)

    run = 1
    while os.path.isfile(loc):
        loc = "aic_synth_data/{0}/aic_{1}_{2}_eps{3}{4}_run{5}.csv".format(synthesizer_name, params, synthesizer_name, eps, prep_eps, str(run))
        run = run + 1

    df.to_csv(loc)

def tt_gan():
    return TableTransformer([
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # aline_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # day_28_flag
        MinMaxTransformer(lower=25, upper=250),# weight_first
        MinMaxTransformer(lower=0, upper=900), # platelet_first
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # renal_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # liver_flag
        MinMaxTransformer(lower=0, upper=60), # tco2_first
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # copd_flag
        MinMaxTransformer(lower=10, upper=200),# map_first
        MinMaxTransformer(lower=100, upper=180), # sodium_first
        MinMaxTransformer(lower=0, upper=100), # wbc_first
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # sofa_first
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # stroke_flag
        MinMaxTransformer(lower=60, upper=140), # chloride_first
        ChainTransformer([LabelTransformer(), OneHotEncoder()]) # service_surg
    ])

def tt_marginal():
    return TableTransformer([
        LabelTransformer(), # gender 1--
        LabelTransformer(), # gender 1--
        BinTransformer(bins=30,lower=25, upper=250),# weight_first
        BinTransformer(bins=30,lower=0, upper=900), # platelet_first
        LabelTransformer(), # gender 1--
        LabelTransformer(), # gender 1--
        BinTransformer(bins=30,lower=0, upper=60), # tco2_first
        LabelTransformer(), # gender 1--
        BinTransformer(bins=30,lower=10, upper=200),# map_first
        BinTransformer(bins=30,lower=100, upper=180), # sodium_first
        BinTransformer(bins=30,ower=0, upper=100), # wbc_first
        LabelTransformer(), # gender 1--
        LabelTransformer(), # gender 1--
        BinTransformer(bins=30,lower=60, upper=140), # chloride_first
        LabelTransformer(), # gender 1--
    ])

def tt_pac():
    return TableTransformer([
        LabelTransformer(), # gender 1--
        LabelTransformer(), # gender 1--
        BinTransformer(bins=30,lower=25, upper=250,nullable=True),# weight_first
        BinTransformer(bins=30,lower=0, upper=900,nullable=True), # platelet_first
        LabelTransformer(), # gender 1--
        LabelTransformer(), # gender 1--
        BinTransformer(bins=30,lower=0, upper=60,nullable=True), # tco2_first
        LabelTransformer(), # gender 1--
        BinTransformer(bins=30,lower=10, upper=200,nullable=True),# map_first
        BinTransformer(bins=30,lower=100, upper=180,nullable=True), # sodium_first
        BinTransformer(bins=30,ower=0, upper=100,nullable=True), # wbc_first
        LabelTransformer(), # gender 1--
        LabelTransformer(), # gender 1--
        BinTransformer(bins=30,lower=60, upper=140,nullable=True), # chloride_first
        LabelTransformer(), # gender 1--
    ])

def gan_dyn(gan_n, eps, prep_eps):
    df = aic_aline()
    for i in range(len(eps)):
        synthesizer = s.create(
            gan_n,
            epsilon = eps[i]
        )
        aic_save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=prep_eps[i]
            ),
            gan_n,
            "dyn",
            eps[i],
            prep_eps[i]
        )

def gan_stat(gan_n, eps, prep_eps):
    df = aic_aline()
    prep_eps = 0.0
    for i in range(len(eps)):
        synthesizer = s.create(
            gan_n,
            epsilon=eps[i]
        )
        aic_save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=0.0,
                transformer=tt_gan()
            ),
            gan_n,
            "stat",
            eps[i]
        )

def pacsynth_dyn(eps, prep_eps):
    df = aic_aline()
    synthesizer = s.create(
        'pacsynth',
        epsilon=eps
    )
    aic_save(
        synthesizer.fit_sample(
            df,
            preprocessor_eps=prep_eps
        ),
        'pacsynth',
        'dyn',
        eps,
        prep_eps
    )

def pacsynth_stat(eps):
    df = aic_aline()
    synthesizer = s.create(
        'pacsynth',
        epsilon=eps,
        delta=1.0 / (math.log(len(df)) * len(df)), # old default
        percentile_percentage=95, # throw out more ouliers than default
        percentile_epsilon_proportion=0.05,
        reporting_length=3 # maximum possible
    )
    aic_save(
        synthesizer.fit_sample(
            df,
            transformer=tt_pac(),
            preprocessor_eps=0.0
        ),
        'pacsynth',
        'stat',
        eps
    )

def mwem_dyn(eps, prep_eps):
    df = aic_aline()
    for i in range(len(eps)):
        synthesizer = s.create(
            'mwem',
            epsilon = eps[i],
            splits = [[0,1,2,3,4,5,6,7],[8,9,10,11,12,13,14]]
            #,split_factor=0,
            ,verbose=True
        )
        aic_save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=prep_eps[i]
            ),
            'mwem',
            'dyn',
            eps[i],
            prep_eps[i]
        )

def mwem_stat(eps, prep_eps):
    df = aic_aline()
    prep_eps = 0.0
    for i in range(len(eps)):
        synthesizer = s.create(
            'mwem',
            epsilon=eps[i]
            #,split_factor=3
            ,verbose=True
        )
        aic_save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=0.0,
                transformer=tt_marginal()
            ),
            'mwem',
            "stat",
            eps[i]
        )

def mst_dyn(eps, prep_eps):
    df = aline()
    for i in range(len(eps)):
        synthesizer = s.create(
            'mst',
            epsilon = eps[i],
        )
        save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=prep_eps[i]
            ),
            'mst',
            'dyn',
            eps[i],
            prep_eps[i]
        )

def mst_stat(eps, prep_eps):
    df = aline()
    prep_eps = 0.0
    for i in range(len(eps)):
        synthesizer = s.create(
            'mst',
            epsilon=eps[i],
        )
        save(
            synthesizer.fit_sample(
                df,
                preprocessor_eps=0.0,
                transformer=tt_marginal()
            ),
            'mst',
            "stat",
            eps[i]
        )


# usage: call with .
# $python main_exec.py <synthesizer> <dyn/stat>
if __name__ == '__main__':

    epsilon = [
        #0.3, # NIST challenge
        #1.0, # NIST challenge
        #3.0, # lots of default epsilons are at 3
        #8.0, # NIST challenge
        20.0 # because we have a lot of columns
    ]

    # preprocessing budget in the usage examples ranged from 50% to 25%
    prep_eps = [
        #0.1,
        #0.3,
        #1.0,
        #3.0,
        5.0
    ]

    run = str(sys.argv[1])
    params = str(sys.argv[2]) # dyn or stat
    if(run not in s.list_synthesizers()): exit("'" + run + "' not a synthesizer")
    if(params not in ["dyn","stat"]): exit("'" + params + "' not understandable have a great day")

    if('gan' in run):
        runs = [None]*10
        for i in range(len(runs)):
            runs[i] = Process(target=eval("gan_" + params), args=(run, epsilon, prep_eps))
        for i in range(len(runs)):
            runs[i].start()

    # PAC does not allow pickeling, so no Process :/
    if('pacsynth' == run):
        for i in range(len(epsilon)):
            for j in range(10):
                if(params == "dyn"):
                    pacsynth_dyn(epsilon[i], prep_eps[i])
                if(params == "stat"):
                    pacsynth_stat(epsilon[i])
                j = j+1

    if('mwem' == run):
        runs = [None]*10
        for i in range(len(runs)):
            if(params == "dyn"):
                runs[i] = Process(target=mwem_dyn, args=(epsilon, prep_eps))
            else:
                runs[i] = Process(target=mwem_stat, args=(epsilon, prep_eps))
        for i in range(len(runs)):
            runs[i].start()

    if('mst' == run):
        runs = [None]*10
        for i in range(len(runs)):
            if(params == "dyn"):
                runs[i] = Process(target=mst_dyn, args=(epsilon, prep_eps))
            else:
                runs[i] = Process(target=mst_stat, args=(epsilon, prep_eps))
        for i in range(len(runs)):
            runs[i].start()
