from snsynth import Synthesizer as s
from snsynth.transform import *

from multiprocessing import Process
import sys

import pandas as pd
import numpy as np

def f_tt_aline():
    return TableTransformer([
        BinTransformer(bins=30, lower=16, upper=92), # age 0--
        LabelTransformer(), # gender 1--
        LabelTransformer(), # day_icu_intime 2--
        LabelTransformer(), # hour_icu_intime 3--
        LabelTransformer(), # icu_exp_flag 4--
        LabelTransformer(), # day_28_flag 5--
        LabelTransformer(), # aline_flag 6--
        BinTransformer(bins=30, lower=10, upper=250),# weight_first 7--
        LabelTransformer(), # service_unit 8--
        LabelTransformer(), # sofa_first 9--
        BinTransformer(bins=30, lower=10, upper=200),# map_first 10--
        BinTransformer(bins=30, lower=25, upper=200),# hr_first 11--
        BinTransformer(bins=30, lower=30, upper=45),# temp_first 12--
        BinTransformer(bins=30, lower=30, upper=100),# spo2_first 13-
        BinTransformer(bins=30, lower=1, upper=150),# bun_first 14--
        BinTransformer(bins=30, lower=0, upper=20), # creatinine_first 15--
        BinTransformer(bins=30, lower=60, upper=140), # chloride_first 16--
        BinTransformer(bins=30, lower=2, upper=25), # hgb_first 17--
        BinTransformer(bins=30, lower=0, upper=900), # platelet_first 18--
        BinTransformer(bins=30, lower=1, upper=10), # potassium_first 19--
        BinTransformer(bins=30, lower=100, upper=180), # sodium_first 20--
        BinTransformer(bins=30, lower=0, upper=60), # tco2_first 21--
        BinTransformer(bins=30, lower=0, upper=100), # wbc_first 22--
        LabelTransformer(), # chf_flag 23 --
        LabelTransformer(), # afib_flag 24 --
        LabelTransformer(), # renal_flag 25 --
        LabelTransformer(), # liver_flag 26 --
        LabelTransformer(), # copd_flag 27 --
        LabelTransformer(), # cad_flag 28--
        LabelTransformer(), # stroke_flag 29--
        LabelTransformer(), # malignancy_flag 30 --
        LabelTransformer() # respfail_flag 31--
    ])

def paramless_run(eps, p_eps, split=4):
    df = pd.read_csv("original_data/aline_clean.csv", index_col=None)
    synthesizer = s.create('mwem', epsilon=eps, split_factor=split, verbose=True)
    synthesizer.fit(df, preprocessor_eps=p_eps)
    sdf = synthesizer.sample(3000)
    sdf.to_csv("synth_data/" + 'dyn_mwem' + "_" + str(synthesizer.epsilon) + "_" + str(p_eps) + ".csv")

# doesnt consume preprocessing budget
def param_run_f(eps):
    df = pd.read_csv("original_data/aline_clean.csv", index_col=None)
    tt_aline = f_tt_aline()
    synthesizer = s.create(
        'mwem',
        epsilon=eps,
        verbose=True,
        #splits=[[1,4,5,6,23,24,25,26,27,28,29,30,31],[0,2,3],[7,8,9],[10,13,14],[11,17,21,16],[12,20,22],[15,18,19]], #keep binary attributes together
        #splits=[[0,4,5,24], [14,15,25,9], [1,2,3,30], [6,8,7,10], [18,23,26,27], [11,12,28,29], [17,19,21,31], [16,20,22,13]],
        #q_count=250,
        #iterations=500,
        split_factor=4,
        add_ranges=True,
    )
    synthesizer.fit(df, transformer=tt_aline, preprocessor_eps=0.0)
    sdf = synthesizer.sample(3000)
    sdf.to_csv("synth_data/" + 'Pmwem' + "_" + str(synthesizer.epsilon) + ".csv")

# generate all mwem sets used in entire thesis:
#   eps = 0.1, 1, 3, 8 with own transformer
#   eps = 3, 8 with automatic transformer consuming epsilon of 1
if (__name__ == "__main__"):
    #eps = float(sys.argv[1])
    #p_eps = float(sys.argv[2])

    #t = f_tt_aline()
    #df = pd.read_csv("original_data/aline_clean.csv", index_col=None)
    #t.

    Process(target=paramless_run, args=[0.2, 0.1]).start()
    #Process(target=paramless_run, args=[1.0, 0.333]).start()
    #Process(target=paramless_run, args=[3.0, 1.0]).start()
    Process(target=paramless_run, args=[8.0, 3.0]).start()

    #param_run_f(eps)
    #paramless_run(eps, p_eps)
