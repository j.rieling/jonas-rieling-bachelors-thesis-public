MWEM_folder = '/../synth_data/opendp_MWEM/'
PKL_folder = '/../synth_data/opendp_MWEM/pkl/'
fast_run = False

import pandas as pd
import numpy as np
from snsynth import MWEMSynthesizer
import pickle
import sys
import os
import pathlib as path
from multiprocessing import Process
from os.path import join

this_path = os.path.dirname(os.path.abspath(__file__))
print(this_path)

#function to save (synthesizer) objects for possible later use, as it takes quite a while to generate them
def save_pkl(obj: object, filename):
    with open(os.path.join(this_path + PKL_folder + filename), 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

#load pkl from pkl folder and return object
def load_pkl(filename: str):
    with open(os.path.join(this_path + PKL_folder + filename), 'rb') as inp:
        loaded_pkl = pickle.load(inp)
    return loaded_pkl

def fit_synthesizer(
        dataframe,
        q_count=400,
        epsilon=3.00,
        iterations=60,
        mult_weights_iterations=40,
        splits=[],
        split_factor=1):

    df = dataframe.copy()
    synthesizer = MWEMSynthesizer(
            q_count=q_count,
            epsilon=epsilon,
            iterations=iterations,
            mult_weights_iterations=mult_weights_iterations,
            splits=splits,
            split_factor=split_factor)

    #fit data to synthesizer
    synthesizer.fit(df.to_numpy())
    #save fitted synthesizer object as pkl
    save_pkl(synthesizer, ('synthesizer' + str(eps) + '.pkl'))

    #returns synthesizer object at address
    return synthesizer

#call script with args </path/to/aline/data.csv> and <epsilon>
#generates 5 samples of original data shape; in folder specified in $MWEM_folder
if __name__ == '__main__':
    file = sys.argv[1]
    eps = float(sys.argv[2])
    
    
    df = pd.read_csv(file).infer_objects()
    if fast_run:
        df = df.sample(4, axis=1)
        df = df.sample(5, axis=0)
        synthesizer = fit_synthesizer(df, epsilon=eps, q_count=200, iterations=10, mult_weights_iterations =5)
    else:
        synthesizer = fit_synthesizer(df, epsilon=eps)
    
    synth_dfs = [None]*5
    run = 1

    #pull 5 samples of original data shape from synthesizer
    for i in range(0,5):
        synth_dfs[i] = pd.DataFrame(synthesizer.sample(df.shape[0]), columns=df.columns)

    #specify path and filename
    filename = this_path + MWEM_folder + "synth_eps{0}_run{1}.csv".format(str(eps), str(run))
    print(filename)

    #save the 5 samples generated above
    for i in  range(0,5):
        #check first if a file with name /path/synth_eps<val>_run<no.>.csv already exists
        while os.path.isfile(filename):
            run+=1
            filename = this_path + MWEM_folder + "synth_eps{0}_run{1}.csv".format(str(eps), str(run))
        synth_dfs[i].to_csv(filename)
        print('saved synthetic data sample in: ' + os.path.normpath(join(os.getcwd(), filename)))

