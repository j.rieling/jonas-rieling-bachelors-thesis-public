from snsynth import Synthesizer as s
from snsynth.transform import *

from multiprocessing import Process
import sys

import pandas as pd
import numpy as np

def tt_aline():
    return TableTransformer([
        MinMaxTransformer(lower=16, upper=92), # age
        LabelTransformer(), # gender
        LabelTransformer(), # day_icu_intime
        LabelTransformer(), # hour_icu_intime
        LabelTransformer(), # icu_exp_flag
        LabelTransformer(), # day_28_flag
        LabelTransformer(), # aline_flag
        MinMaxTransformer(lower=10, upper=250),# weight_first
        LabelTransformer(), # service_unit
        LabelTransformer(), # sofa_first
        MinMaxTransformer(lower=10, upper=200),# map_first
        MinMaxTransformer(lower=25, upper=200),# hr_first
        MinMaxTransformer(lower=30, upper=45),# temp_first
        MinMaxTransformer(lower=30, upper=100),# spo2_first
        MinMaxTransformer(lower=1, upper=150),# bun_first
        MinMaxTransformer(lower=0, upper=20), # creatinine_first
        MinMaxTransformer(lower=60, upper=140), # chloride_first
        MinMaxTransformer(lower=2, upper=25), # hgb_first
        MinMaxTransformer(lower=0, upper=900), # platelet_first
        MinMaxTransformer(lower=1, upper=10), # potassium_first
        MinMaxTransformer(lower=100, upper=180), # sodium_first
        MinMaxTransformer(lower=0, upper=60), # tco2_first
        MinMaxTransformer(lower=0, upper=100), # wbc_first
        LabelTransformer(), # chf_flag
        LabelTransformer(), # afib_flag
        LabelTransformer(), # renal_flag
        LabelTransformer(), # liver_flag
        LabelTransformer(), # copd_flag
        LabelTransformer(), # cad_flag
        LabelTransformer(), # stroke_flag
        LabelTransformer(), # malignancy_flag
        LabelTransformer() # respfail_flag
    ])

def run(s, df, p_eps, name):
    print(name)
    s.fit(df, preprocessor_eps=p_eps)
    sdf = s.sample(2000)
    sdf.to_csv("synth_data/" + name + "_" + str(s.epsilon) + "_" + str(p_eps) + ".csv")

# pre-processing steps prior to synthesizing with this file:
#  - rounding (for memory)
#  - logical sanity
if (__name__ == "__main__"):
    df = pd.read_csv("original_data/aline_clean.csv", index_col=None)
    eps = float(sys.argv[1])
    p_eps = float(sys.argv[2])

    used_synths = s.list_synthesizers()
    #cannot be run through Process.start()
    used_synths.remove('pacsynth')


    synthesizers = [None]*len(used_synths)
    processes = [None]*len(synthesizers)
    dataframes = [None]*len(synthesizers)


    i = 0
    for n in used_synths:
        if(n == 'mwem'):
            # sadly 
            synthesizers[i] = s.create(synth=n, epsilon=eps, split_factor=3)
        else:
            synthesizers[i] = s.create(synth=n, epsilon=eps)

        i = i+1

    # not able to run seperately
    pac_synthesizer = s.create(synth='pacsynth', epsilon=eps)
    pac_synthesizer.fit(df, preprocessor_eps=p_eps)
    sdf_pac = pac_synthesizer.sample(2000)
    sdf_pac.to_csv("synth_data/" + "pacsynth" + "_" + str(pac_synthesizer.epsilon) + "_" + str(p_eps) + ".csv")

    for i in range(len(processes)):
        print(str(i) + ": " + used_synths[i] + " - " + str(synthesizers[i]))
        processes[i] = Process(target=run, args=(synthesizers[i], df, p_eps, used_synths[i]))
        processes[i].start()



    #synth = s.create(synth='mst', epsilon=3.0, verbose=True)
    #synthetic = #synth.fit_sample(dft, preprocessor_eps=1.0)
