from snsynth import Synthesizer as s
from snsynth.transform import *

from multiprocessing import Process
import sys

import pandas as pd
import numpy as np

def tt_aline():
    return TableTransformer([
        MinMaxTransformer(lower=16, upper=92), # age
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # gender
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # day_icu_intime
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # hour_icu_intime
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # icu_exp_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # day_28_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # aline_flag
        MinMaxTransformer(lower=25, upper=250),# weight_first
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # service_unit
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # sofa_first
        MinMaxTransformer(lower=10, upper=200),# map_first
        MinMaxTransformer(lower=25, upper=200),# hr_first
        MinMaxTransformer(lower=30, upper=45),# temp_first
        MinMaxTransformer(lower=30, upper=100),# spo2_first
        MinMaxTransformer(lower=1, upper=150),# bun_first
        MinMaxTransformer(lower=0, upper=20), # creatinine_first
        MinMaxTransformer(lower=60, upper=140), # chloride_first
        MinMaxTransformer(lower=2, upper=25), # hgb_first
        MinMaxTransformer(lower=0, upper=900), # platelet_first
        MinMaxTransformer(lower=1, upper=10), # potassium_first
        MinMaxTransformer(lower=100, upper=180), # sodium_first
        MinMaxTransformer(lower=0, upper=60), # tco2_first
        MinMaxTransformer(lower=0, upper=100), # wbc_first
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # chf_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # afib_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # renal_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # liver_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # copd_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # cad_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # stroke_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]), # malignancy_flag
        ChainTransformer([LabelTransformer(), OneHotEncoder()]) # respfail_flag
    ])
#
# def paramless_run(eps, p_eps, split=4):
#     df = pd.read_csv("original_data/aline_clean.csv", index_col=None)
#     synthesizer = s.create('mwem', epsilon=eps, split_factor=split, verbose=True)
#     synthesizer.fit(df, preprocessor_eps=p_eps)
#     sdf = synthesizer.sample(3000)
#     sdf.to_csv("synth_data/" + 'dyn_mwem' + "_" + str(synthesizer.epsilon) + "_" + str(p_eps) + ".csv")
def dyn_run_f(eps, pp_eps, synthesizer_n):
    df = pd.read_csv("original_data/aline_clean.csv", index_col=None)
    synthesizer = s.create(
        synthesizer_n,
        epsilon=eps,
        #verbose=True,
        #splits=[[1,4,5,6,23,24,25,26,27,28,29,30,31],[0,2,3],[7,8,9],[10,13,14],[11,17,21,16],[12,20,22],[15,18,19]], #keep binary attributes together
        #splits=[[0,4,5,24], [14,15,25,9], [1,2,3,30], [6,8,7,10], [18,23,26,27], [11,12,28,29], [17,19,21,31], [16,20,22,13]],
        #q_count=250,
        #iterations=500,
        #split_factor=4,
        #add_ranges=True,
    )
    synthesizer.fit(df, preprocessor_eps=pp_eps)
    sdf = synthesizer.sample(3000)
    sdf.to_csv("synth_data/dyn_" + synthesizer_n + "_" + str(eps) + "_pp" + str(pp_eps) + ".csv")


# doesnt consume preprocessing budget
def param_run_f(eps, synthesizer_n):
    df = pd.read_csv("original_data/aline_clean.csv", index_col=None)
    synthesizer = s.create(
        synthesizer_n,
        epsilon=eps,
        #verbose=True,
        #splits=[[1,4,5,6,23,24,25,26,27,28,29,30,31],[0,2,3],[7,8,9],[10,13,14],[11,17,21,16],[12,20,22],[15,18,19]], #keep binary attributes together
        #splits=[[0,4,5,24], [14,15,25,9], [1,2,3,30], [6,8,7,10], [18,23,26,27], [11,12,28,29], [17,19,21,31], [16,20,22,13]],
        #q_count=250,
        #iterations=500,
        #split_factor=4,
        #add_ranges=True,
    )
    synthesizer.fit(df, transformer=tt_aline(), preprocessor_eps=0.0)
    sdf = synthesizer.sample(3000)
    sdf.to_csv("synth_data/stat_" + synthesizer_n + "_" + str(eps) + ".csv")

# pre-processing steps prior to synthesizing with this file:
#  - rounding (for memory)
#  - logical sanity
if (__name__ == "__main__"):
    df = pd.read_csv("original_data/aline_clean.csv", index_col=None)
    eps = float(sys.argv[1])
    #pp_eps = float(sys.argv[2])

    gans = [
        'dpctgan',
        'patectgan',
        'dpgan',
        'pategan'
        ]

    for s in gans:
        Process(target=param_run_f, args=(eps, s)).start()




    # used_synths = s.list_synthesizers()
    # #cannot be run through Process.start()
    # used_synths.remove('pacsynth')
    #
    #
    # synthesizers = [None]*len(used_synths)
    # processes = [None]*len(synthesizers)
    # dataframes = [None]*len(synthesizers)
    #
    #
    # i = 0
    # for n in used_synths:
    #     if(n == 'mwem'):
    #         # sadly
    #         synthesizers[i] = s.create(synth=n, epsilon=eps, split_factor=3)
    #     else:
    #         synthesizers[i] = s.create(synth=n, epsilon=eps)
    #
    #     i = i+1
    #
    # # not able to run seperately
    # pac_synthesizer = s.create(synth='pacsynth', epsilon=eps)
    # pac_synthesizer.fit(df, preprocessor_eps=p_eps)
    # sdf_pac = pac_synthesizer.sample(2000)
    # sdf_pac.to_csv("synth_data/" + "pacsynth" + "_" + str(pac_synthesizer.epsilon) + "_" + str(p_eps) + ".csv")
    #
    # for i in range(len(processes)):
    #     print(str(i) + ": " + used_synths[i] + " - " + str(synthesizers[i]))
    #     processes[i] = Process(target=run, args=(synthesizers[i], df, p_eps, used_synths[i]))
    #     processes[i].start()
    #
    #
    #
    # #synth = s.create(synth='mst', epsilon=3.0, verbose=True)
    # #synthetic = #synth.fit_sample(dft, preprocessor_eps=1.0)
