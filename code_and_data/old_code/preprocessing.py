import pandas as pd
import sys

dictionary_gender = {
    "F": 1,
    "M": 0,}
dictionary_day_icu_intime = {
    "monday   ": 1,
    "tuesday  ": 2,
    "wednesday": 3,
    "thursday ": 4,
    "friday   ": 5,
    "saturday ": 6,
    "sunday   ": 7,}
dictionary_service_unit = {
    "NMED": 1,
    "MED": 2,
    "NSURG": 3,
    "TRAUM": 4,
    "ORTHO": 5,
    "SURG": 6,
    "ENT": 7,
    "OMED": 8,
    "PSURG": 9,
    "GU": 10,
    "GYN": 11,
    "DEN": 12,
    "OBS": 13,
    "DENT": 14,}
    
#commented out rows, that don't rely too much on the decimals for the sake of memory usage and processing time
float_cols = {
    #'age', # 16 - 91y
    #'hour_icu_intime', # 0-23h
    #'weight_first', # 26 - 230kg
    #'map_first', # 22 - 259
    #'hr_first', # 35 - 174
    'temp_first', # 33 - 40.44
    #'spo2_first', # 67 - 100
    #'bun_first', # 1 - 139
    'creatinine_first', # 0.1 - 18.8
    #'chloride_first', # 73 - 129
    'hgb_first', # 4.1 - 19.8
    #'platelet_first', # 6 - 781
    'potassium_first', # 1.8 - 7.3
    #'sodium_first', # 112 - 165
    #'tco2_first', # 3 - 53
    #'wbc_first', # 0.2 -94
}

# takes a python dictionary and inverses it
def inverse_dictionary(d):
    return {v: k for k, v in d.items()}

# decode / encode dataframe from MIMIC aline cohort for synthesizer processing
#
#  floats are converted to int, because MWEM and other synthesizers dont take float values, as they are known to harm otherwise proven theoretical privacy guarantees
#  the choice of factor 10 to keep float information was forced, because of exploding in-memory histogram size (when using MWEM) when choosing higher values
def de_encode(df, task: str, float_fac=10.0):
    if task not in ['dec', 'enc']: sys.exit('wrong argument in task parameter')
#
    #cleaning previously encoded data would falsely set most ages to 91
    rdf = df

    #decide dictionary direction
    rep_gender = inverse_dictionary(dictionary_gender) if (task == 'dec') else dictionary_gender
    rep_day_icu_intime = inverse_dictionary(dictionary_day_icu_intime) if (task == 'dec') else dictionary_day_icu_intime
    rep_service_unit = inverse_dictionary(dictionary_service_unit) if (task == 'dec') else dictionary_service_unit

    #function for encoding/ decoding float columns
    mult = (lambda x : x/float_fac) if (task == 'dec') else (lambda x: x*float_fac)

    #string columns
    rdf['gender'] = rdf['gender'].replace(rep_gender)
    rdf['day_icu_intime'] = rdf['day_icu_intime'].replace(rep_day_icu_intime)
    rdf['service_unit'] = rdf['service_unit'].replace(rep_service_unit)

    #set types accordingly ('enc'->all int, 'dec'->mixed)
    for col in float_cols:
        rdf[col] = rdf[col].astype(float)
        rdf[col] = rdf[col].apply(mult)
    for col in rdf.columns:
        if(task == 'enc'):
            rdf[col] = rdf[col].astype(int)
        else:
            if not (col in float_cols or col in {'gender', 'day_icu_intime', 'service_unit'}):
                rdf[col] = rdf[col].astype(int)
    return rdf

#When script is executed directly:
# arg[1] = path/to/csv
# arg[2] = <dec/enc> decides wether csv is decoded or encoded for processing by synthesizer
# file is being saved at same path as the input file as <filename>_<dec/enc>.csv
if __name__ == '__main__':
    import pathlib as path

    #weak check for argument validity
    if len(sys.argv) != 3 or (sys.argv[2] not in ['dec', 'enc']): sys.exit('wrong arguments')

    file = sys.argv[1]
    task = sys.argv[2]
    df = pd.read_csv(file, sep=',', encoding='utf-8').infer_objects()

    df = de_encode(df, task)

    p = path.Path(file)
    newf = "{0}_{2}{1}".format(path.Path.joinpath(p.parent, p.stem), p.suffix, task)
    df.to_csv(newf, index=False)

    # file = sys.argv[1]
    # df = pd.read_csv(file, sep=',', encoding='utf-8').infer_objects()
    #
    # df = clean(df)
    #
    # p = path.Path(file)
    # newf = "{0}_{2}{1}".format(path.Path.joinpath(p.parent, p.stem), p.suffix, 'cleaned')
    # df.to_csv(newf, index=False)
