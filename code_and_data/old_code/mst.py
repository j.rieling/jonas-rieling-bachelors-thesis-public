from snsynth import Synthesizer as s
from snsynth.transform import *

from multiprocessing import Process
import sys

import pandas as pd
import numpy as np

def paramless_run(eps, p_eps):
    df = pd.read_csv("original_data/aline_data.csv", index_col=None)
    synthesizer = s.create('mst', epsilon=eps, verbose=True)
    sdf = synthesizer.fit_sample(df, preprocessor_eps=p_eps)
    #sdf = synthesizer.sample(3000)
    sdf.to_csv("synth_data/" + "sample_mst" + ".csv")

# doesnt consume preprocessing budget


if (__name__ == "__main__"):
    #eps = float(sys.argv[1])
    #p_eps = float(sys.argv[2])

    #t = f_tt_aline()
    #df = pd.read_csv("original_data/aline_clean.csv", index_col=None)

    #Process(target=paramless_run, args=[0.2, 0.1]).start()
    #Process(target=paramless_run, args=[1.0, 0.333]).start()
    #Process(target=paramless_run, args=[3.0, 1.0]).start()
    Process(target=paramless_run, args=[8.0, 3.0]).start()


    #param_run_f(eps)
    #paramless_run(eps, p_eps)
