from snsynth import Synthesizer as s
from snsynth.transform import *

from multiprocessing import Process
import sys

import pandas as pd
import numpy as np

def aline():
    return pd.read_csv("original_data/aline_clean.csv", index_col=None)
    
def add_nans(df):
    df = df.append([None], ignore_index=True)
    df.drop(0, inplace=True, axis=1)
    return df

def f_tt_aline():
    return TableTransformer([
        BinTransformer(bins=30, lower=16, upper=92,nullable=True), # age 0--
        LabelTransformer(nullable=True), # gender 1--
        LabelTransformer(nullable=True), # day_icu_intime 2--
        LabelTransformer(nullable=True), # hour_icu_intime 3--
        LabelTransformer(nullable=True), # icu_exp_flag 4--
        LabelTransformer(nullable=True), # day_28_flag 5--
        LabelTransformer(nullable=True), # aline_flag 6--
        BinTransformer(bins=30, lower=10, upper=250, nullable=True),# weight_first 7--
        LabelTransformer(nullable=True), # service_unit 8--
        LabelTransformer(nullable=True), # sofa_first 9--
        BinTransformer(bins=30, lower=10, upper=200, nullable=True),# map_first 10--
        BinTransformer(bins=30, lower=25, upper=200, nullable=True),# hr_first 11--
        BinTransformer(bins=30, lower=30, upper=45, nullable=True),# temp_first 12--
        BinTransformer(bins=30, lower=30, upper=100, nullable=True),# spo2_first 13-
        BinTransformer(bins=30, lower=1, upper=150, nullable=True),# bun_first 14--
        BinTransformer(bins=30, lower=0, upper=20, nullable=True), # creatinine_first 15--
        BinTransformer(bins=30, lower=60, upper=140, nullable=True), # chloride_first 16--
        BinTransformer(bins=30, lower=2, upper=25, nullable=True), # hgb_first 17--
        BinTransformer(bins=30, lower=0, upper=900, nullable=True), # platelet_first 18--
        BinTransformer(bins=30, lower=1, upper=10, nullable=True), # potassium_first 19--
        BinTransformer(bins=30, lower=100, upper=180, nullable=True), # sodium_first 20--
        BinTransformer(bins=30, lower=0, upper=60, nullable=True), # tco2_first 21--
        BinTransformer(bins=30, lower=0, upper=100, nullable=True), # wbc_first 22--
        LabelTransformer(nullable=True), # chf_flag 23 --
        LabelTransformer(nullable=True), # afib_flag 24 --
        LabelTransformer(nullable=True), # renal_flag 25 --
        LabelTransformer(nullable=True), # liver_flag 26 --
        LabelTransformer(nullable=True), # copd_flag 27 --
        LabelTransformer(nullable=True), # cad_flag 28--
        LabelTransformer(nullable=True), # stroke_flag 29--
        LabelTransformer(nullable=True), # malignancy_flag 30 --
        LabelTransformer(nullable=True) # respfail_flag 31--
        ]
    )

def dyn_mwem(eps, pp_eps):

def stat_mwem(eps):


if (__name__ == "__main__"):
    #eps = float(sys.argv[1])
    #p_eps = float(sys.argv[2])

    #t = f_tt_aline()
    #df = pd.read_csv("original_data/aline_clean.csv", index_col=None)


    #param_run_f(eps)
