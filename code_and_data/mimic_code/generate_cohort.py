# code okay w/ update

import sys
import pandas as pd
import psycopg2
import os

#change to con_object.py and edit w/ own credentials
from con_object_local import get_credentials

# wrapper for sql queries
def execute_query(sql, con):
	cur = con.cursor()
	try:
		cur.execute("set search_path to mimiciii;" + sql)
	except:
		cur.execute('rollback;')
		raise
	finally:
		cur.close()
	return

con = get_credentials()
p = os.path.dirname(os.path.abspath(__file__))

#load datediff function for postgres
execute_query(''.join(open(p + '/sql_code/date_diff.sql').readlines()), con)

#arranged queries from subfolders
#SQL code from: git MIT-LCP/mimic-code/mimiciii /concepts and /notebooks/aline
queries_sorted = [
	p+'/sql_code/sepsis/angus_sepsis.sql',
	p+'/sql_code/demographics/heightweightquery.sql',
	p+'/sql_code/aline_vaso_flag.sql',
	p+'/sql_code/durations/vent_settings.sql',
	p+'/sql_code/durations/ventdurations.sql',
    p+'/sql_code/aline_cohort_all.sql',
    p+'/sql_code/aline_cohort.sql',
	p+'/sql_code/independent_aline/aline_bmi.sql',
	p+'/sql_code/independent_aline/aline_sofa.sql',
	p+'/sql_code/independent_aline/aline_labs.sql',
	p+'/sql_code/independent_aline/aline_sedatives.sql',
	p+'/sql_code/independent_aline/aline_icd.sql',
	p+'/sql_code/independent_aline/aline_vitals.sql',
]

for query_loc in queries_sorted:
    query = ''.join(open(query_loc).readlines())
    execute_query(query, con);

df = pd.read_sql_query(''.join(open(p+'/sql_code/final_query.sql').readlines()), con)

#indices not used in MIT/mimic-code/samples/aline "aline_propensity_score.Rmd"
unnecessary_indices = [
    'subject_id',
    'hadm_id',
    'icustay_id',
    'icustay_intime',
    'icu_hour_flag',
    'icustay_outtime',
    'icu_los_day',
    'hospital_los_day',
    'hosp_exp_flag',
    'mort_day',
    'mort_day_censored',
    'censor_flag',
    'aline_time_day',
    'height_first',
    'bmi',
    'endocarditis_flag',
    'ards_flag',
    'pneumonia_flag',
    'sedative_flag',
    'midazolam_flag',
    'fentanyl_flag',
    'propofol_flag',
]

for i in unnecessary_indices:
    df.drop(columns=i, inplace=True)

df.to_csv(p+'/../original_data/aline_data.csv',index=False)

#clean tables
drop_all = """
	DROP MATERIALIZED VIEW IF EXISTS ALINE_COHORT CASCADE;
	DROP MATERIALIZED VIEW IF EXISTS ALINE_COHORT_ALL CASCADE;
    DROP MATERIALIZED VIEW IF EXISTS ALINE_BMI CASCADE;
    DROP MATERIALIZED VIEW IF EXISTS ALINE_ICD CASCADE;
    DROP MATERIALIZED VIEW IF EXISTS ALINE_LABS CASCADE;
    DROP MATERIALIZED VIEW IF EXISTS ALINE_SEDATIVES CASCADE;
    DROP MATERIALIZED VIEW IF EXISTS ALINE_SOFA CASCADE;
    DROP MATERIALIZED VIEW IF EXISTS ALINE_VITALS CASCADE;
    DROP TABLE IF EXISTS angus_sepsis;
    DROP TABLE IF EXISTS ventsettings;
    DROP TABLE IF EXISTS ventdurations;
    DROP TABLE IF EXISTS heightweight;
    DROP TABLE IF EXISTS ALINE_VASO_FLAG;
""";
execute_query(drop_all, con)
con.close()
