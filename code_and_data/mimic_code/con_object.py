import psycopg2

def get_credentials():
    return psycopg2.connect(
        dbname='mimic',
        user='<username>',
        password='<password>',
        host='<host>')
