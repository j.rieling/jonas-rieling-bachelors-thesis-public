# Jonas Rieling - Bachelors Thesis


All synthesizers work so far on Fedora36-Server via python venv module:
```
$ dnf install gcc rustc cargo git python3.8
$ python3.8 -m venv .venv
$ source .venv/bin/activate
# now in active venv
$ pip install install smartnoise-synth scikit-learn scikit-image seaborn requests tqdm opendp diffprivlib pyspark openmdao git+https://github.com/ryan112358/private-pgm.git jax jaxlib
$ python main_exec.py <synthesizer> <dyn/stat>
```



Docker not used yet in thesis, however the image works -- do:
```
$ docker build -t venv .
$ docker run -it --name test --mount type=bind,source="$(pwd)",target=/app venv
```
and run code inside
