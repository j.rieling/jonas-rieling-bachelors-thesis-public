import pandas as pd
import numpy as np
#from snsynth import MWEMSynthesizer
import pickle
import sys
import os
import pathlib as path
from multiprocessing import Process
from os.path import join

PKL_folder = '/../synth_data/pkl/'
SynthD_folder = '/../synth_data/'
this_path = os.path.dirname(os.path.abspath(__file__)) # path/to/py_util

#function to save (synthesizer) objects for possible later use, as it takes quite a while to generate them
def save_pkl(obj: object, filename):
    with open(os.path.join(this_path + PKL_folder + filename), 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

#load pkl from pkl folder and return object
def load_pkl(filename: str):
    with open(os.path.join(this_path + PKL_folder + filename), 'rb') as inp:
        loaded_pkl = pickle.load(inp)
    return loaded_pkl

# saves dataframe to ../synth_data
def save_synthetic_dataset(synthesizer_name: str, df, epsilon):
    run = 1
    filename = this_path + SynthD_folder + synthesizer_name + "_eps{0}_run{1}".format(str(epsilon), str(run))
    while os.path.isfile(filename):
        run+=1
        filename = this_path + SynthD_folder + synthesizer_name + "_eps{0}_run{1}".format(str(epsilon), str(run))
    df.to_csv(filename)
